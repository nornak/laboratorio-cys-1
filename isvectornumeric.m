

function respuesta = isvectornumeric(vector)

	respuesta = 1;
	largo = length(vector);
	cont = 1;

	while(cont<=largo)

		if(~isnumeric(vector(cont)))

			respuesta = 0;
			break;
	
		end

		cont = cont + 1;	
	end

end




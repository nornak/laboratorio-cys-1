% Gráfica la función a(x)

function grafico_a

    % asigna a gnuplot como 'toolkit'
    % solo para el uso en octave
    if (is_octave)
        graphics_toolkit ("gnuplot");
    end

    % desde 0 a 4*pi, con saltos de 0.01
    x = 0:0.01:4*pi;
    % arreglo con los resultados de la funcion a(x)
    y = funcion_a(x);

    % dibuja los punto de x e y, de color azul y con puntos
    plot(x, y, 'b.');

    % titulo del grafico
    title('Funcion a(x)');
    % etiqueta para los ejes
    xlabel('Tiempo');
    ylabel('a(x)');
    % activa la rejilla
    grid;
    % pone una legenda
    legend(' a(x) ');

end


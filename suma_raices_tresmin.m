

function suma = suma_raices_tresmin(vector)

	%se verifica si se ingresa solo un parametro 
	if (nargin ~= 1) 
	
		disp('ERROR: Debe ingresar un y solo un argumento a la funcion');

		return 

	end

	%verifica si el argumento es valido
	if(es_argumento_valido(vector))

		%se ordena el vector
		vec = sort(vector);

		%se realiza la suma de las reices de los menores tres valores
		suma = sqrt(vec(1)) + sqrt(vec(2)) + sqrt(vec(3));
	end

end

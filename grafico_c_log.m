%Funcion que grafica la ecuacion c(x) en esacala logaritmica
function grafico_c_log
	
    % asigna a gnuplot como 'toolkit'
    % solo para el uso en octave
    if (is_octave)
        graphics_toolkit ("gnuplot");
    end

	%rango de puntos para evaluar la funcion
	x=-30:0.05:30;

	%llamado de la funcion c(x)
	y=funcion_c(x);
	
	%funcion que grafica en escala logaritmica la funcion y=c(x)
	%en los puntos x
	semilogy(x,y,'b');

	%formato del grafico
	title('Grafica a escala logaritmica de c(x)');
	xlabel('x');	
	ylabel('c(x)');
	grid;

end

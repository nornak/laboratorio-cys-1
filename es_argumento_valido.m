%Funcion que verifica la validez del argumento ingresado

function boolean = es_argumento_valido(vector)

	
	%se verifica si el argumento es un vector
	if (isvector(vector)) 
	    
		%se verifica si el argumento es un vector con tamaño minimo tres
		if(isvectornumeric(vector))	
	
			%si es un vector numerico, ejecutar calculo
			if(length(vector)>=3)
				
				boolean = 1;

				return
	
			else
					
				disp('ERROR: El vector ingresado no posee como minimo tres elementos');			
				boolean = 0;

				return

			end

		else
		
			
			disp('ERROR: El vector contiene valores no numericos');
			boolean = 0;

			return
		end

	else
		
	    disp('ERROR: El argumento ingresado no es un vector');
		boolean = 0;

		return
	end


end

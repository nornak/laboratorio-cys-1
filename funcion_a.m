% funcion de la ecuacion a(x)
% x: recibe un valor y lo evalua en la ecuacion
% Retorno: la funcion evaluada en el argumento
function y = funcion_a(x)
    y = 13*cambiar_base(7*x-8, 3);
end

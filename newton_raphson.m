% Funcion que hace el metodo de newton - rapshon para calcular las raices o una
% aproximacion a ellas

% pol: polinomio de entrada
% n: cantidad de iteraciones
% e: error a considerar
% x1: valor inicial

function salida =  newton_raphson(pol, n, e, x1)

    % si pol es un numero escalar, aborta
    if isscalar(pol)
        error("El primer atributo debe ser un polinomio")
    end

    % si hay iteraciones
    if n > 0
        % evalual el polinomio pol en x1
        f = polyval(pol, x1);
        % evalua el polinomio derivado con el valor x1
        g = polyval(polyder(pol), x1);

        % parte de la ecuacion de newton rapshon
        div = f / g;
        x2 = x1 - div;

        % calcula el error
        err = abs(calc_error(x1, x2));

        % si el error ingresado es menor que el calculado, seguir calculando
        if e <= err
            salida = newton_raphson(pol, n - 1, e, x2);
        else
            salida = x2;
        end
    else
        salida = x1;
    end
end


% calcula el error relativo
% x1: valor anterior 
% x2: valor actual
function err = calc_error(x1,x2)
    if x2 ~= 0
        err = (x2 - x1) / x2;
    else
        err = 0;
    end
end

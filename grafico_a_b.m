% funcion que grafica a(x) y b(x) en un mismo grafico

function grafico_a_b

    % asigna a gnuplot como 'toolkit'
    % solo para el uso en octave
    if (is_octave)
        graphics_toolkit ("gnuplot");
    end

    % desde 0 a 4*pi, con saltos de 0.01
    x = 0:0.01:4*pi;
    % valores de la funcion a(x)
    y1 = funcion_a(x);
    % valores de la funcion b(x)
    y2 = funcion_b(x);

    % grafica los puntos de la funcion a(x) y b(x)
    % el primer conjunto tiene color b y los puntos son el caracter '.'
    % el segundo conjunto tiene color r y los puntos son el caracter 'x'
    plot(x, y1, 'b.',x, y2, 'rx');

    % titulo del grafico
    title('funcion a(x) y b(x)');
    % etiqueta para los ejes
    xlabel('Tiempo');ylabel('');
    % activa la rejilla
    grid;
    % pone una legenda
    legend('a(x)', 'b(x)');

end


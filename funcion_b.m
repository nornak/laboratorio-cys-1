% funcion de la ecuacion b(x)
% x: recibe un valor y lo evalua en la ecuacion
% Retorno: la funcion evaluada en el argumento
function y = funcion_b(x)
    y = cos(3*cambiar_base(17*x+5, 7));
end

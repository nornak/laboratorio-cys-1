% Gráfica la función b(x)
function grafico_b

    % asigna a gnuplot como 'toolkit'
    % solo para el uso en octave
    if (is_octave)
        graphics_toolkit ("gnuplot");
    end

    % x va desde 0 a 4*pi con saltos de 0.01
    x = 0:0.01:4*pi;
    % resultado de la función b(x) 
    y = funcion_b(x);

    % dibuja los puntos del resultado de la funcion b(x)
    % los puntos son del color rojo y muestran el caracter x, ademas se cambiar
    % el tamaño de los caracteres
    plot(x, y, 'rx', 'markersize', 3);

    % titulo del grafico
    title('funcion b(x)');
    % etiquetas para los ejes
    xlabel('Tiempo');
    ylabel('b(x)');
    % muestra la rejilla
    grid;
    % muestra una legenda
    legend(' b(x)  ');

end


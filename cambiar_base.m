% Funcion que realiza el cambio de base en los logaritmos.
% x: valor que se desea calcular.
% base: la base con la cual se calcula el logaritmo.
function valor = cambiar_base(x, base)

    valor = log10(x) ./ log10(base);

end

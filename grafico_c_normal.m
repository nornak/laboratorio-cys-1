%Funcion que grafica la ecuacion c(x) en escala normal
function grafico_c_normal
	
	%rango de puntos para evaluar la funcion
	x=-30:0.05:30;

	%llamado de la funcion c(x)
	y=funcion_c(x);

	%funcion que grafica en escala logaritmica la funcion y=c(x)
	plot(x,y,'r');

	%Formato del grafico
	title('Grafica a escala normal de c(x)');
	xlabel('x');	
	ylabel('c(x)');
	grid;

end
